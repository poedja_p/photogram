﻿using RadyaLabs.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photogram
{
    public class Photo : ModelBase
    {

        public Photo()
        {
            photoTimestamp = DateTime.Now;
        }

        public string id { get; set; }

        private string _photoUrl;
        public string photoUrl
        {
            get
            {
                return _photoUrl;
            }
            set
            {
                _photoUrl = value;
                NotifyPropertyChanged("photoUrl");
            }
        }

        private string _caption;
        public string caption
        {
            get
            {
                return _caption;
            }
            set
            {
                _caption = value;
                NotifyPropertyChanged("caption");
            }
        }

        private string _userId;
        public string userId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
                NotifyPropertyChanged("userId");
            }
        }

        private DateTime _photoTimestamp;
        public DateTime photoTimestamp
        {
            get
            {
                return _photoTimestamp;
            }
            set
            {
                _photoTimestamp = value;
                NotifyPropertyChanged("photoTimestamp");
            }
        }

     
    }
}
