﻿using RadyaLabs.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photogram
{
    public class Comment : ModelBase
    {

        public Comment()
        {

        }

        public string id { get; set; }

        private string _message;
        public string message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
                NotifyPropertyChanged("message");
            }
        }

      
        private string _userId;
        public string userId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
                NotifyPropertyChanged("userId");
            }
        }

        private string _photoId;
        public string photoId
        {
            get
            {
                return _photoId;
            }
            set
            {
                _photoId = value;
                NotifyPropertyChanged("photoId");
            }
        }

    }
}
