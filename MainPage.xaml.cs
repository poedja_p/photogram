﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Photogram.Resources;
using Microsoft.WindowsAzure.MobileServices;
using RadyaLabs.Common;
using Microsoft.Azure.Engagement;

namespace Photogram
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            EngagementAgent.Instance.StartActivity("MainPage");

            Load(false);

            

        }

        private void OnCameraClicked(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/AddPhoto.xaml", UriKind.Relative));
        }

        public async void Load(bool isLocal)
        {
            if (isLocal)
            {
                #region Dummy Service

                List<Photo> listOfPhoto = new List<Photo>();
                Photo photo1 = new Photo()
                {
                    caption = "Hi! I'm a new photo",
                    photoUrl = "http://blackdesk.io/wp-content/uploads/2015/04/10653330_10153190376468618_2126229184739880684_n.jpg",
                    userId = "poedja"
                };

                Photo photo2 = new Photo()
                {
                    caption = "Mode error can happen, this is grapes",
                    photoUrl = "http://blackdesk.io/wp-content/uploads/2015/04/TBJ4OW9XG9X-1200x800.jpg",
                    userId = "poedja"
                };


                Photo photo3 = new Photo()
                {
                    caption = "Can this coding standard be you",
                    photoUrl = "http://blackdesk.io/wp-content/uploads/2015/03/LV2IUQNTZ52-1200x800.jpg",
                    userId = "poedja"
                };

                Photo photo4 = new Photo()
                {
                    caption = "Age of Ultron",
                    photoUrl = "http://blackdesk.io/wp-content/uploads/2015/02/1498520_22-1200x750.jpg",
                    userId = "poedja"
                };

                listOfPhoto.Add(photo1);
                listOfPhoto.Add(photo2);
                listOfPhoto.Add(photo3);
                listOfPhoto.Add(photo4);


                listOfPhoto.Add(photo1);
                listOfPhoto.Add(photo2);
                listOfPhoto.Add(photo3);
                listOfPhoto.Add(photo4);

                TimelineView.ItemsSource = listOfPhoto;
                GridView.ItemsSource = listOfPhoto;

                #endregion
            }
            else { 
                //azure mobile service here..
                MobileServiceClient client = new MobileServiceClient(ConfigHelper.MobileServiceUrl, ConfigHelper.MobileServiceKey);

                //azure auth here..
                MobileServiceUser user = new MobileServiceUser(SettingHelper.GetKeyValue<string>("USERID"));
                user.MobileServiceAuthenticationToken = SettingHelper.GetKeyValue<string>("USERTOKEN");

                client.CurrentUser = user;
               
                
                var photoTable = client.GetTable<Photo>();
                try
                {
                    var result = await photoTable.ToCollectionAsync();

                    TimelineView.ItemsSource = result.Reverse().ToList();
                    GridView.ItemsSource = result.Reverse().ToList();
                }
                catch (Exception ex)
                { 
                
                }
            }
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var list = (sender as LongListSelector);

            PhoneApplicationService.Current.State["CURRENT_PHOTO"] = list.SelectedItem as Photo;
            NavigationService.Navigate(new Uri("/View/DetailPhoto.xaml", UriKind.Relative));
        }




    }
}