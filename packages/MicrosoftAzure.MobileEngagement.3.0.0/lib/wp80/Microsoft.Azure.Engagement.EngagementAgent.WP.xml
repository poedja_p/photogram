<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Microsoft.Azure.Engagement.EngagementAgent.WP</name>
    </assembly>
    <members>
        <member name="T:Microsoft.Azure.Engagement.DeviceStatus">
            <summary>
            Device status.
            </summary>
        </member>
        <member name="F:Microsoft.Azure.Engagement.DeviceStatus.REGEX_PHONE">
            <summary>
            Contain a regex to identify a phone number.
            Simply look for a plus sign followed by a sequence of digits/spaces/parenthesis/dash.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.#ctor">
            <summary>
            Create a new device status.
            Should be run in a task.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.GetTechnicals">
            <summary>
            Get all technical information regarding the device.
            </summary>
            <returns>An array conataining all technicals.</returns>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.GetDeviceId">
            <summary>
            Get the device unique identifier.
            </summary>
            <returns>The unique device identifer.</returns>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.GetScreenSize">
            <summary>
            Get the screen size.
            Synchrone method, can take up to 2s.
            </summary>
            <returns>A WxH string, where W is the width, and H the height.</returns>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.SetSDKVersion">
            <summary>
            SDK version.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.SetFirmware">
            <summary>
            Device firmware.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.SetPhoneModel">
            <summary>
            Phone model.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.SetPhoneManufacturer">
            <summary>
            Phone manufacturer.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.SetNetwork">
            <summary>
            Network.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.SetCarrier">
            <summary>
            Carrier.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.SetLocale">
            <summary>
            Locale.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.SetTimezoneOffset">
            <summary>
            Timezone offset.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.SetScreenSize">
            <summary>
            Screen size.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceStatus.SetApplicationVersionName">
            <summary>
            Application version name.
            </summary>
        </member>
        <member name="T:Microsoft.Azure.Engagement.NetworkStatus">
            <summary>
            INetworkStatus implementation.
            </summary>
        </member>
        <member name="T:Microsoft.Azure.Engagement.EngagementAgent">
            <summary>
            Engagement Agent for Windows Phone.
            </summary>
        </member>
        <member name="F:Microsoft.Azure.Engagement.EngagementAgent.ENGAGEMENT_CONFIGURATION_PATH">
            <summary>
            Path to the Engagement configuration file.
            </summary>
        </member>
        <member name="F:Microsoft.Azure.Engagement.EngagementAgent.sInstance">
            <summary>
            Unique instance of Engagement agent.
            </summary>
        </member>
        <member name="F:Microsoft.Azure.Engagement.EngagementAgent.syncInstantiation">
            <summary>
            Lock instanciation for thread safety.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.EngagementAgent.OnActivated(Microsoft.Phone.Shell.ActivatedEventArgs)">
            <summary>
            Restore Engagement agent state after an application activation.
            </summary>
            <param name="e">The activation event.</param>
        </member>
        <member name="M:Microsoft.Azure.Engagement.EngagementAgent.#ctor">
            <summary>
            Create a Engagement Agent.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.EngagementAgent.Init(Microsoft.Azure.Engagement.EngagementConfiguration)">
            <summary>
            Init the Engagement Agent module.
            You should call this method when your application starts.
            </summary>
            <param name="EngagementConfiguration">Your engagement configuration object if the EngagementConfiguration.xml file is not set.</param>
            <exception cref="T:Microsoft.Azure.Engagement.EngagementIncorrectConfigurationException">This exception is thrown if the Engagement configuration is not set or is not valid.</exception>
        </member>
        <member name="P:Microsoft.Azure.Engagement.EngagementAgent.AgentInitTask">
            <summary>
            Gets the agent initialization task.
            </summary>
            <value>
            The agent initialization task.
            </value>
        </member>
        <member name="P:Microsoft.Azure.Engagement.EngagementAgent.Instance">
            <summary>
            Get the unique Engagement Agent instance.
            </summary>
        </member>
        <member name="T:Microsoft.Azure.Engagement.EngagementPage">
            <summary>
            Helper class used to replace Windows Phone's PhoneApplicationPage class.
            Automatically starts a new Engagement activity when displayed.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.EngagementPage.OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs)">
            <summary>
            Send an activity to the engagement backend when the page is displayed (not loaded).
            </summary>
            <param name="e">Navigation event argument.</param>
        </member>
        <member name="M:Microsoft.Azure.Engagement.EngagementPage.GetEngagementPageName">
            <summary>
            Override this to specify the name reported by your page. 
            The default implementation returns the simple name of the class 
            and removes the "Page" suffix if any (e.g. "MainPage" -> "Main").
            </summary>
            <returns>The page name reported by the Engagement service.</returns>
        </member>
        <member name="M:Microsoft.Azure.Engagement.EngagementPage.GetEngagementPageExtra">
            <summary>
            Override this to attach extra information to your page. 
            The default implementation attaches no extra information (i.e. return null).
            </summary>
            <returns>Page extra information, null or empty if no extra.</returns>
        </member>
        <member name="T:Microsoft.Azure.Engagement.ApplicationLifecycleManager">
            <summary>
            Application life cycle management for Windows Phone.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.ApplicationLifecycleManager.SetOnSuspendEventHandler(System.Action)">
            <summary>
            Set the suspended handler.
            </summary>
            <param name="onSuspended">Callback when the app has been suspended.</param>
        </member>
        <member name="M:Microsoft.Azure.Engagement.ApplicationLifecycleManager.SetOnCrashedHandler(System.Action{System.Exception})">
            <summary>
            Set the crashed handler.
            </summary>
            <param name="onCrashed">Callback when the app has been crashed.
            Takes one parameter : the crash string format.</param>
        </member>
        <member name="M:Microsoft.Azure.Engagement.ApplicationLifecycleManager.StoreInDeviceState">
            <summary>
            Store the engagement configuration in the device RAM to recover after a suspended state.
            </summary>
        </member>
        <member name="T:Microsoft.Azure.Engagement.DeviceIdentification">
            <summary>
            Device identification manager.
            </summary>
        </member>
        <member name="F:Microsoft.Azure.Engagement.DeviceIdentification.sDeviceId">
            <summary>
            Device id.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DeviceIdentification.GetDeviceId">
            <summary>
            Find the device id of the current device
            </summary>
            <returns>The device id as a 128 bits SHA256</returns>
        </member>
        <member name="T:Microsoft.Azure.Engagement.DefaultSHA256">
            <summary>
            Utility class to wrap the Sha256 algorithm.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.DefaultSHA256.Hash128(System.String)">
             <summary>
             Hash data with Sha256 then truncate output to 128 bits.
             </summary>
            <param name="data">data to hash.</param>
             <returns>hashed data in hexadecimal output truncated to 128 bits.</returns>
        </member>
        <member name="T:Microsoft.Azure.Engagement.WindowsPhoneAgentFactory">
            <summary>
            Windows Phone AgentFactory implementation.
            </summary>
        </member>
        <member name="M:Microsoft.Azure.Engagement.WindowsPhoneAgentFactory.CreateApplicationLifecycleManager">
            <summary>
            Return a unique sInstance of an application lifecycle manager.
            </summary>
            <returns>
            Unique sInstance of an application lifecycle manager
            </returns>
        </member>
    </members>
</doc>
