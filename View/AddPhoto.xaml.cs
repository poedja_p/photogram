﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using RadyaLabs.Common;
using Microsoft.Azure.Engagement;

namespace Photogram.View
{
    public partial class AddPhoto : PhoneApplicationPage
    {
        CameraCaptureTask camera = new CameraCaptureTask();
        string fileName = null;
        Stream fileStream = null;

        public AddPhoto()
        {
            InitializeComponent();
            camera.Completed += camera_Completed;
        }

        void camera_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                //add to preview
                fileName = e.OriginalFileName;
                BitmapImage image = new BitmapImage();
                image.SetSource(e.ChosenPhoto);
                ImagePreview.Source = image;
               
                e.ChosenPhoto.Seek(0, SeekOrigin.Begin);

                fileStream = e.ChosenPhoto;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            
            base.OnNavigatedTo(e);
            EngagementAgent.Instance.StartActivity("AddPhoto");

            if (e.NavigationMode != NavigationMode.Back)
            {
                camera.Show();   
            }
        }

        private async void OnShareTapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //add azure mobile service

            //create object
            Photo photo = new Photo();
            photo.caption = TextBoxCaption.Text;
            photo.userId = SettingHelper.GetKeyValue<string>("USERID").Split(':')[1];
            photo.photoTimestamp = DateTime.Now;


            await UploadToCloudAsync(fileStream, photo);
        }

        private async Task UploadToCloudAsync(Stream photoStream,Photo photo)
        {
            try
            {
                var photoUrl = await UploadToBlobAsync(photoStream);
                if (!String.IsNullOrEmpty(photoUrl))
                {
                    photo.photoUrl = photoUrl;
                    MobileServiceClient client = new MobileServiceClient(ConfigHelper.MobileServiceUrl, ConfigHelper.MobileServiceKey);
                    var photoTable = client.GetTable<Photo>();
                    await photoTable.InsertAsync(photo);

                    NavigationService.GoBack();
                }
            }
            catch (Exception ex)
            { 
            
            }
        }


        private async Task<string> UploadToBlobAsync(Stream stream)
        {
            try
            {
                string fileName = Guid.NewGuid().ToString() + ".jpg";
                StorageCredentials creds = new StorageCredentials(ConfigHelper.StorageName, ConfigHelper.StorageKey);
                CloudStorageAccount account = new CloudStorageAccount(creds, true);
                CloudBlobClient client = account.CreateCloudBlobClient();
                CloudBlobContainer sampleContainer = client.GetContainerReference(ConfigHelper.StorageContainer);
                CloudBlockBlob blob = sampleContainer.GetBlockBlobReference(fileName);
                await blob.UploadFromStreamAsync(stream);



                return String.Format(ConfigHelper.PhotoUrlFormat, fileName);

            }
            catch (Exception ex)
            {
                return null;
            }
            

        }


        /*
         * 
                function insert(item, user, request) {
                // Define a payload for the Windows Phone toast notification.
                var payload = '<?xml version="1.0" encoding="utf-8"?>' +
                    '<wp:Notification xmlns:wp="WPNotification"><wp:Toast>' +
                    '<wp:Text1>New photo is arrived, it says</wp:Text1><wp:Text2>' + item.caption + 
                    '</wp:Text2></wp:Toast></wp:Notification>';

                request.execute({
                    success: function() {
                        // If the insert succeeds, send a notification.
                        push.mpns.send(null, payload, 'toast', 22, {
                            success: function(pushResponse) {
                                console.log("Sent push:", pushResponse);
                                request.respond();
                                },              
                                error: function (pushResponse) {
                                    console.log("Error Sending push:", pushResponse);
                                    request.respond(500, { error: pushResponse });
                                    }
                                });
                            }
                        });      
                }
         */

    }
}