﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.WindowsAzure.MobileServices;
using RadyaLabs.Common;
using Microsoft.Azure.Engagement;

namespace Photogram.View
{
    public partial class LoginPage : PhoneApplicationPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            EngagementAgent.Instance.StartActivity("LoginPage");

        }

        private async void OnLoginClicked(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //add azure auth here..
            try
            {
                MobileServiceClient client = new MobileServiceClient(ConfigHelper.MobileServiceUrl, ConfigHelper.MobileServiceKey);
                var user = await client.LoginAsync(MobileServiceAuthenticationProvider.Facebook);
               

                SettingHelper.SetKeyValue<string>("USERID", user.UserId);
                SettingHelper.SetKeyValue<string>("USERTOKEN", user.MobileServiceAuthenticationToken);
                SettingHelper.SetKeyValue<bool>("ISLOGIN", true);



                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
            catch (InvalidOperationException ex)
            { 
            
            }

        }
    }
}