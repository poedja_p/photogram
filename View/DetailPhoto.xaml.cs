﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.Azure.Engagement;
using RadyaLabs.Common;

namespace Photogram.View
{
    public partial class DetailPhoto : PhoneApplicationPage
    {

        public DetailPhoto()
        {
            InitializeComponent();
        }

        

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            EngagementAgent.Instance.StartActivity("DetailPhoto");

            
            
            if (PhoneApplicationService.Current.State.ContainsKey("CURRENT_PHOTO"))
            {
                DataContext = PhoneApplicationService.Current.State["CURRENT_PHOTO"] as Photo;
                Load(false);
            }
        }

        public async void Load(bool isLocal)
        {
            if (isLocal)
            {
                #region Dummy Service

                List<Comment> listOfComment = new List<Comment>();

                Comment comment1 = new Comment()
                {
                 userId = "poedja", message = "nice photo bro!"
                };

                Comment comment2 = new Comment()
                {
                    userId = "poedja",
                    message = "photo nya mayanlah ok!"
                };

                Comment comment3 = new Comment()
                {
                    userId = "poedja",
                    message = "ajib!"
                };

                listOfComment.Add(comment1);
                listOfComment.Add(comment2);
                listOfComment.Add(comment3);


                CommentView.ItemsSource = listOfComment;
                #endregion
            }
            else
            {
                //azure mobile service here..
                Photo currentPhoto = DataContext as Photo;
                MobileServiceClient client = new MobileServiceClient(ConfigHelper.MobileServiceUrl, ConfigHelper.MobileServiceKey);
                var commentTable = client.GetTable<Comment>();
                var result = await commentTable.Where(item => item.photoId == currentPhoto.id).ToCollectionAsync(); 

                CommentView.ItemsSource = result;


            }
        }

        private async void OnSendComment(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //azure mobile service here..
            MobileServiceClient client = new MobileServiceClient(ConfigHelper.MobileServiceUrl, ConfigHelper.MobileServiceKey);
            var commentTable = client.GetTable<Comment>();


            Comment comment = new Comment();
            comment.message = TextBoxComment.Text;
            comment.photoId = (DataContext as Photo).id;
            comment.userId = SettingHelper.GetKeyValue<string>("USERID").Split(':')[1];

            await commentTable.InsertAsync(comment);

            Load(false);
        }
    }
}